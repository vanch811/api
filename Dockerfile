FROM python:3.9.0-alpine

EXPOSE 5000

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
COPY . .
RUN python -m pip install -r requirements.txt

WORKDIR /app
COPY . /app

CMD ["python", "manage.py"]