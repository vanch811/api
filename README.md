## word replacement API

### Main endpoint **GET** `/`
Simple webpage displaying a "Welcome to word replacement API. Do a POST call to get the actual result".

### Copyright symbol endpoint **POST** `/api/companies/copyrighted`
Send a JSON POST request to the API which contains one or more companies in a string which need the copyright symbol added. Example:
```JSON
{
    "input": "We really like the new security features of Google Cloud"
}
```
